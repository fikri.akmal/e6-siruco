
from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from e6_siruco.auth_decorator import require_login


@require_login
def update_transaksi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('idtransaksi') is not None:
                idtransaksi = request.GET.get('idtransaksi')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.TRANSAKSI_RS where idtransaksi=%s LIMIT 1;', [idtransaksi])
                    data = dictfetchall(cursor)
                data_pasien = {}
                data_pasien['idtransaksi'] = data[0]['idtransaksi']
                data_pasien['kodepasien'] = data[0]['kodepasien']
                if(data[0]['waktupembayaran'] != None):
                    data_pasien['waktu_pembayaran'] = data[0]['waktupembayaran']
                    data_pasien['tanggal_pembayaran'] = data[0]['tanggalpembayaran']

                else:
                    data_pasien['waktu_pembayaran'] = 'Belum Bayar'
                    data_pasien['tanggal_pembayaran'] = 'Belum Bayar'

                data_pasien['tanggal_pembayaran'] = data[0]['tanggalpembayaran']
                data_pasien['tanggal_masuk'] = str(data[0]['tglmasuk'])
                data_pasien['total_biaya'] = data[0]['totalbiaya']
                data_pasien['status_bayar'] = data[0]['statusbayar']
                form = UpdateTransaksiRSForm(initial=data_pasien)
        else:
            idtransaksi = request.POST['idtransaksi']
            statusbayar = request.POST['status_bayar']

            data_to_update = [statusbayar, idtransaksi]
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.TRANSAKSI_RS SET statusbayar = %s WHERE idtransaksi = %s", data_to_update)
            return redirect('transaksirs:read')
        return render(request, 'transaksirs/updateTransaksiRS.html', {'form': form})


@require_login
def delete_transaksi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('idtransaksi') is not None:
                idtransaksi = request.GET.get('idtransaksi')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'DELETE FROM SIRUCO.TRANSAKSI_RS WHERE idtransaksi = %s', [idtransaksi])
        return HttpResponseRedirect(reverse('transaksirs:read'))


@require_login
def read_transaksi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_RS")
            seluruh = dictfetchall(cursor)
        context = {'rrst': seluruh}
        return render(request, 'transaksirs/readTransaksiRS.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
