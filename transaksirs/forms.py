from django import forms
from django.db import connection


class UpdateTransaksiRSForm(forms.Form):
    idtransaksi = forms.CharField(
        label=("ID Transaksi"), required=True, max_length=3)
    kodepasien = forms.CharField(
        label=("Kode Pasien"), required=True, max_length=30)
    tanggal_masuk = forms.CharField(
        label=("Tanggal Masuk"), required=True, max_length=30)
    tanggal_pembayaran = forms.CharField(
        label=("Tanggal Pembayaran"), required=True, max_length=30)
    waktu_pembayaran = forms.CharField(
        label=("Waktu Pembayaran"), required=True, max_length=30)
    total_biaya = forms.CharField(
        label=("Total Biaya"), required=True, max_length=30)
    status_bayar = forms.CharField(label=("Status Bayar"),
                                   required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateTransaksiRSForm, self).__init__(*args, **kwargs)
        self.fields['idtransaksi'].widget.attrs['readonly'] = True
        self.fields['tanggal_masuk'].widget.attrs['readonly'] = True
        self.fields['kodepasien'].widget.attrs['readonly'] = True
        self.fields['tanggal_pembayaran'].widget.attrs['readonly'] = True
        self.fields['waktu_pembayaran'].widget.attrs['readonly'] = True
        self.fields['total_biaya'].widget.attrs['readonly'] = True
