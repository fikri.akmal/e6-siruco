from django import forms
from django.db import connection


class CreateReservasiRSForm(forms.Form):
    nik_list = []
    nik = forms.ChoiceField(choices=nik_list, required=True)
    tanggal_masuk = forms.DateField(label=(
        "Tanggal Masuk"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_masuk'}))
    tanggal_keluar = forms.DateField(label=(
        "Tanggal Keluar"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_keluar'}))
    kode_rs_list = []
    kode_rs = forms.ChoiceField(
        label=('Kode RS'), choices=kode_rs_list, required=True, widget=forms.Select(attrs={'id': 'kode_rs'}))

    def __init__(self, *args, **kwargs):
        super(CreateReservasiRSForm, self).__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT NIK,NIK FROM SIRUCO.PASIEN")
            self.fields['nik'].choices = cursor.fetchall()
            cursor.execute(
                "SELECT kode_faskes,kode_faskes FROM SIRUCO.rumah_sakit ORDER BY KODE_FASKES ASC")
            self.fields['kode_rs'].choices = cursor.fetchall()


class UpdateReservasiRSForm(forms.Form):
    nikpasien = forms.CharField(
        label=("Kode Faskes"), required=True, max_length=3)
    tanggal_masuk = forms.DateField(label=(
        "Tanggal Masuk"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_masuk'}))
    tanggal_keluar = forms.DateField(label=(
        "Tanggal Keluar"), required=True, widget=forms.DateInput(attrs={'type': 'date', 'id': 'tanggal_keluar'}))
    koders = forms.CharField(
        label=("Kode RS"), required=True, max_length=30)
    koderuangan = forms.CharField(
        label=("Kode Ruangan"), required=True, max_length=30)
    kodebed = forms.CharField(label=("Kode Bed"),
                              required=True, max_length=30)

    def __init__(self, *args, **kwargs):
        super(UpdateReservasiRSForm, self).__init__(*args, **kwargs)
        self.fields['nikpasien'].widget.attrs['readonly'] = True
        self.fields['tanggal_masuk'].widget.attrs['readonly'] = True
        self.fields['koders'].widget.attrs['readonly'] = True
        self.fields['koderuangan'].widget.attrs['readonly'] = True
        self.fields['kodebed'].widget.attrs['readonly'] = True
