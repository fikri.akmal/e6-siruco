from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from datetime import datetime
from e6_siruco.auth_decorator import require_login


def ajaxRuanganRS(request):
    kode_faskes = request.GET.get('kode_faskes')
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT KODERUANGAN FROM SIRUCO.RUANGAN_RS WHERE KODERS = %s AND KODERUANGAN NOT IN (SELECT KODERUANGAN FROM SIRUCO.RESERVASI_RS)", [kode_faskes])
        data = cursor.fetchall()
    list_ruangan = []
    for i in data:
        list_ruangan.append(i[0])

    return JsonResponse({'id': list_ruangan})


def ajaxBedRS(request):
    kode_faskes = request.GET.get('kode_faskes')
    kode_ruangan = request.GET.get('kode_ruangan')
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT KODEBED FROM SIRUCO.BED_RS WHERE KODERS = %s AND KODERUANGAN = %s AND KODEBED NOT IN (SELECT KODEBED FROM SIRUCO.RESERVASI_RS WHERE KODERS = %s AND KODERUANGAN = %s);", [kode_faskes, kode_ruangan, kode_faskes, kode_ruangan])
        data = cursor.fetchall()
    list_bed = []
    for i in data:
        list_bed.append(i[0])

    return JsonResponse({'id': list_bed})


@require_login
def create_reservasi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        form = CreateReservasiRSForm()
        if request.method == "POST":
            kode_pasien = request.POST['nik']
            tglmasuk = request.POST['tanggal_masuk']
            tglkeluar = request.POST['tanggal_keluar']
            koders = request.POST['kode_rs']
            koderuangan = request.POST['ruanganRS']
            kodebed = request.POST['bed']

            data_baru = [kode_pasien, tglmasuk,
                         tglkeluar, koders, koderuangan, kodebed]
            print(data_baru)
            with connection.cursor() as cursor:
                cursor.execute(
                    'INSERT INTO SIRUCO.RESERVASI_RS VALUES (%s, %s, %s, %s, %s, %s)', data_baru)
            return redirect('reservasiRS:read')
        return render(request, 'reservasiRS/createReservasiRS.html', {'form': form})


@require_login
def update_reservasi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            print(request.GET.get('kodepasien'), request.GET.get('tglmasuk'))
            if request.GET.get('kodepasien') is not None and request.GET.get('tglmasuk') is not None:
                print(23232)
                kodepasien = request.GET.get('kodepasien')
                tglmasuk = request.GET.get('tglmasuk')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.RESERVASI_RS where kodepasien = %s AND tglmasuk = %s LIMIT 1;', [kodepasien, tglmasuk])
                    data = dictfetchall(cursor)
                data_pasien = {}
                data_pasien['nikpasien'] = data[0]['kodepasien']
                data_pasien['tanggal_masuk'] = data[0]['tglmasuk']
                data_pasien['tanggal_keluar'] = data[0]['tglkeluar']
                data_pasien['koders'] = data[0]['koders']
                data_pasien['koderuangan'] = data[0]['koderuangan']
                data_pasien['kodebed'] = data[0]['kodebed']
                form = UpdateReservasiRSForm(initial=data_pasien)
        else:
            kodepasien = request.POST['nikpasien']
            tglkeluar = request.POST['tanggal_keluar']
            tglmasuk = request.POST['tanggal_masuk']
            data_to_update = [tglkeluar, kodepasien, tglmasuk]
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.RESERVASI_RS SET tglkeluar = %s WHERE KODEPASIEN = %s and TGLMASUK = %s", data_to_update)
            return redirect('reservasiRS:read')
        return render(request, 'reservasiRS/updateReservasiRS.html', {'form': form})


@require_login
def delete_reservasi_rs(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('kodepasien') is not None and request.GET.get('tglmasuk') is not None:
                kodepasien = request.GET.get('kodepasien')
                tglmasuk = request.GET.get('tglmasuk')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'DELETE FROM SIRUCO.RESERVASI_RS WHERE kodepasien = %s AND tglmasuk = %s', [kodepasien, tglmasuk])
        return HttpResponseRedirect(reverse('reservasiRS:read'))


@require_login
def read_reservasiRS(request):
    # Jika bukan pengguna publik
    if request.session.get('peran', False) == 'admin satgas':
         with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RESERVASI_RS")
            seluruh = dictfetchall(cursor)
            context = {'apt': seluruh,
                       'date': datetime.today().strftime('%Y-%m-%d')}
    if request.session.get('peran', False) == 'pengguna publik':
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT * FROM SIRUCO.RESERVASI_RS WHERE KODEPASIEN IN (SELECT NIK FROM SIRUCO.PASIEN WHERE IDPENDAFTAR =%s)", [request.session['username']])
            seluruh = dictfetchall(cursor)
            context = {'apt': seluruh,
                       'date': datetime.today().strftime('%Y-%m-%d')}
    # else if request.session.get('peran', False) != 'admin satgas' or request.session.get('peran', False) != 'pengguna publik':
    #     return redirect('authentication:index')
    return render(request, 'reservasiRS/readReservasiRS.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
