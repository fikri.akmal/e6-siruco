 $(document).ready(function () {
        $('#formCreate').on('submit', function (e) {
            const tgl_masuk = $('#tanggal_masuk').val();
            const tgl_keluar = $('#tanggal_keluar').val();
            console.log(tgl_masuk)
            console.log(tgl_keluar)
            if (tgl_masuk > tgl_keluar) {
                alert('Tanggal masuk tidak boleh lebih dari tanggal keluar!')
                e.preventDefault();
            }
        })

        $.ajax({
            type: 'GET',
            url: "{% url 'reservasiRS:ajaxRuanganRS' %}",
            data: {
                kode_faskes: $('#kode_rs').val()
            },
            async: false,
            success: function (response) {
                response.id.map((e) => {
                    $('#ruanganRS').append(new Option(e, e))
                })
            }
        });

        $.ajax({
            type: 'GET',
            url: "{% url 'reservasiRS:ajaxBedRS' %}",
            data: {
                kode_faskes: $('#kode_rs').val(),
                kode_ruangan: $('#ruanganRS').val(),
            },
            async: false,
            success: function (response) {
                response.id.map((e) => {
                    $('#bed').append(new Option(e, e))
                })
            }
        });

        $('#kode_rs').on('change', function () {
            $('#ruanganRS').find('option').remove()
            $.ajax({
                type: 'GET',
                url: "{% url 'reservasiRS:ajaxRuanganRS' %}",
                data: {
                    kode_faskes: $('#kode_rs').val()
                },
                async: false,
                success: function (response) {
                    response.id.map((e) => {
                        $('#ruanganRS').append(new Option(e, e))
                    })
                }
            });
            $('#bed').find('option').remove()
            $.ajax({
                type: 'GET',
                url: "{% url 'reservasiRS:ajaxBedRS' %}",
                data: {
                    kode_faskes: $('#kode_rs').val(),
                    kode_ruangan: $('#ruanganRS').val(),
                },
                async: false,
                success: function (response) {
                    response.id.map((e) => {
                        $('#bed').append(new Option(e, e))
                    })
                }
            });

        });

        $('#ruanganRS').on('change', function () {
            $('#bed').find('option').remove()
            $.ajax({
                type: 'GET',
                url: "{% url 'reservasiRS:ajaxBedRS' %}",
                data: {
                    kode_faskes: $('#kode_rs').val(),
                    kode_ruangan: $('#ruanganRS').val(),
                },
                async: false,
                success: function (response) {
                    response.id.map((e) => {
                        $('#bed').append(new Option(e, e))
                    })
                }
            });
        });

    })
