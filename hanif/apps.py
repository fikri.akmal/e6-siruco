from django.apps import AppConfig


class HanifConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hanif'
