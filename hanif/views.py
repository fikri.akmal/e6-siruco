import json
from django.shortcuts import render
from django.contrib import messages
from django.db import connection
from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from e6_siruco.auth_decorator import require_login

def toRp(angka):
    return f'Rp {int(angka):,}'.replace(",",".") + ",00"

@require_login
def transRSView(request):
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT NIK FROM PASIEN
            WHERE IdPendaftar = '{request.session['username']}'
        """)

        try:
            nik = cursor.fetchall()[0][0]
        except:
            return redirect('main:home')

        cursor.execute(f"""
            SELECT idtransaksi, kodepasien, tanggalpembayaran,
            tglmasuk, totalbiaya, statusbayar, waktupembayaran FROM TRANSAKSI_RS
            WHERE kodepasien = '{nik}'
        """)

        row = cursor.fetchall()

        transRSData = [
            (item[0], item[1], item[2], item[3], toRp(item[4]), item[5], item[6]) for item in row
        ]
        
        context = {
            "transRSData" : transRSData,
        }

    return render(request, "viewTransRS.html", context=context)

@csrf_exempt
def detailRsvRS(request):
    data = json.loads(request.body)

    with connection.cursor() as cursor:
        cursor.execute(f"""
                SELECT tglmasuk, tglkeluar, koders, f.nama, koderuangan, 
                kodebed FROM RESERVASI_RS JOIN FASKES f ON koders = f.kode
                WHERE kodepasien = '{data['kodepasien']}'
                AND tglmasuk = '{data['tglmasuk']}'
            """)

        row = cursor.fetchall()

        rsvRSData = {
            "tglmasuk": row[0][0],
            "tglkeluar": row[0][1],
            "koders": row[0][3] + " - " + row[0][2],
            "koderuangan": row[0][4],
            "kodebed": row[0][5],
        }
    
    return JsonResponse(rsvRSData) 

@csrf_exempt
def konfirmasiRS(request):
    data = json.loads(request.body)

    with connection.cursor() as cursor:
        cursor.execute(f"""
                UPDATE TRANSAKSI_RS SET statusbayar = 'Lunas'
                WHERE idtransaksi = '{data["idtransaksi"]}'
            """)
    return HttpResponse("OK")

@require_login
def transHotelView(request):
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT NIK FROM PASIEN
            WHERE IdPendaftar = '{request.session['username']}'
        """)

        try:
            nik = cursor.fetchall()[0][0]
        except:
            return redirect('main:home')

        cursor.execute(f"""
            SELECT idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran,
            totalbayar, statusbayar FROM TRANSAKSI_HOTEL
            WHERE kodepasien = '{nik}'
        """)

        row = cursor.fetchall()

        transHotelData = [
            (item[0], item[1], item[2], item[3], toRp(item[4]), item[5]) for item in row
        ]
        
    context = {
        "transHotelData" : transHotelData,
    }

    return render(request, "viewTransHotel.html", context=context)

@csrf_exempt
def detailRsvHotel(request):
    data = json.loads(request.body)

    with connection.cursor() as cursor:
        cursor.execute(f"""
                SELECT rh.tglmasuk, rh.tglkeluar, h.nama, rh.koderoom FROM RESERVASI_HOTEL rh
                JOIN HOTEL h on h.kode = rh.kodehotel
                JOIN TRANSAKSI_BOOKING tb on tb.idtransaksibooking = '{data["transID"]}'
                WHERE rh.kodepasien = '{data["kodepasien"]}'
                AND rh.tglmasuk = tb.tglmasuk;
            """)

        row = cursor.fetchall()

        result = {
            "tglmasuk": row[0][0],
            "tglkeluar": row[0][1],
            "nama": row[0][2],
            "koderoom": row[0][3],
        }
        
        cursor.execute(f"""
                SELECT totalbayar FROM TRANSAKSI_BOOKING
                WHERE idtransaksibooking = '{data["transID"]}'
            """)

        row = cursor.fetchall()
        
        result["transbook"] = toRp(row[0][0])

        cursor.execute(f"""
                SELECT totalbayar FROM TRANSAKSI_MAKAN
                WHERE idtransaksi = '{data["transID"]}'
            """)

        row = cursor.fetchall()
        
        transMakanData = [
            (toRp(item[0])) for item in row
        ]

        result["transmakan"] = transMakanData
    
    return JsonResponse(result)

@csrf_exempt
def konfirmasiHotel(request):
    data = json.loads(request.body)

    with connection.cursor() as cursor:
        cursor.execute(f"""
                UPDATE TRANSAKSI_HOTEL SET statusbayar = 'Lunas'
                WHERE idtransaksi = '{data["idtransaksi"]}'
            """)

    return HttpResponse("OK")

@require_login
def tesView(request):
    if request.session['peran'] == 'dokter':
        with connection.cursor() as cursor:
            cursor.execute(f"""
                    SELECT NIK FROM PASIEN
                """)

            row = cursor.fetchall()
        
        context = {
            "pasienData" : row,
        }

        return render(request, "viewTes.html", context=context)
    else:
        return redirect('main:home')

def createHasilTes(request):
    with connection.cursor() as cursor:
        cursor.execute(f"""
            INSERT INTO TES(nik_pasien, tanggaltes, jenis, status, nilaict)
            VALUES ('{request.POST["nik"]}', '{request.POST["tgltes"]}','{request.POST["jenistes"]}',
            '{request.POST["status"]}', '{request.POST["CT"]}');
            """)
    return redirect('hanif:hasilTesView')

@require_login
def hasilTesView(request):
    with connection.cursor() as cursor:
        if request.session['peran'] == 'dokter':
            cursor.execute(f"""
                    SELECT * FROM TES
                """)
        elif request.session['peran'] == 'pengguna publik':
            cursor.execute(f"""
                SELECT NIK FROM PASIEN
                WHERE IdPendaftar = '{request.session['username']}'
            """)

            try:
                nik = cursor.fetchall()[0][0]
            except:
                return redirect('main:home')

            cursor.execute(f"""
                    SELECT * FROM TES WHERE nik_pasien = '{nik}'
                """)
        else:
            return redirect('main:home')

        row = cursor.fetchall()
            
        transHasilData = [
                (no, item[0], item[1], item[2], item[3], item[4]) for (no, item)
                in enumerate(row, start=1)
        ]

    context = {
        "transHasilData" : transHasilData,
    }

    return render(request, "viewHasilTes.html", context=context)
    

