from django.urls import path
from . import views

app_name = "hanif"

urlpatterns = [
    path("transaksiRS/", views.transRSView, name="transRSView"),
    path("detailTransRS/", views.detailRsvRS, name="detailTransRS"),
    path("konfirmasiRS/", views.konfirmasiRS, name="konfirmasiRS"),
    path("transaksiHotel/", views.transHotelView, name="transHotelView"),
    path("detailTransHotel/", views.detailRsvHotel, name="detailTransHotel"),
    path("konfirmasiHotel/", views.konfirmasiHotel, name="konfirmasiHotel"),
    path("formHasilTes/", views.tesView, name="tesView"),
    path("createHasilTes/", views.createHasilTes, name="createHasilTes"),
    path("hasilTes/", views.hasilTesView, name="hasilTesView"),
]
