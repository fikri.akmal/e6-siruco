from django.http.response import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import render
from e6_siruco.auth_decorator import require_login
from e6_siruco.db_conn_util import open_connection
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

# --------- CREATE VIEW
@csrf_exempt
@require_login
def create_transaksi_makan(request):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute("SET search_path to siruco;")

    if request.session.get('peran') == "pengguna publik":
        username = request.session.get('username')
        context = {}
        cursor.execute(
            f"""
            SELECT p.nik, p.idpendaftar, rsh.kodehotel FROM pasien p
            JOIN reservasi_hotel rsh on
            p.nik = rsh.kodepasien;
            """
        )
        list_reservasi = cursor.fetchall()
        has_reservation = False

        # kode pasien dan hotelnya diwakilkan satu indeks yang sama
        kode_pasien = []
        kode_hotel = []

        for i in list_reservasi:
            if username == i[1]:
                has_reservation = True
                kode_pasien.append(i[0])
                kode_hotel.append(i[2])
        
        # ngambil semua id transaksi hotel pasien yang direserve
        list_id_transaksi_hotel = []
        for i in range(len(kode_pasien)):
            cursor.execute(
                f"""
                SELECT idtransaksi from transaksi_hotel
                WHERE kodepasien = '{kode_pasien[i]}';
                """
            )
            list_id_transaksi_hotel.extend(cursor.fetchall())

        context['has_reservation'] = has_reservation

        if has_reservation:
            context['list_id'] = list_id_transaksi_hotel

            # Ambil yang teratas dulu
            context['kode_hotel'] = kode_hotel[0]

            # Ambil id transaksi makan terbesar + 1
            cursor.execute(
                f"""
                SELECT MAX(idtransaksimakan) FROM transaksi_makan
                WHERE idtransaksi = '{list_id_transaksi_hotel[0][0]}';
                """
            )
            latest_id_TM = cursor.fetchall()[0][0]
            new_id_TM = "TM" + str(int(latest_id_TM[2:]) + 1).zfill(4)
            context['newTM'] = new_id_TM

            # Query Ambil List Paket Makan dari Hotel X
            cursor.execute(
                f"""
                SELECT kodehotel, kodepaket, nama FROM paket_makan
                WHERE kodehotel='{kode_hotel[0]}';
                """
            )

            list_paket = cursor.fetchall()
            context['list_paket'] = list_paket

            return render(request, 'buat-transaksi-makan.html', context)


        return render(request, "buat-transaksi-makan.html", context)

    elif request.session.get('peran') == "admin satgas":
        # ambil semua id transaksi dan kodepasien dari transaksi hotel
        cursor.execute("SELECT idtransaksi, kodepasien from transaksi_hotel")

        list_id = cursor.fetchall()
        list_id.sort(key=lambda x:x[0])

        # ambil kode hotel dari idtransaksihotel paling pertama
        kodepasien = list_id[0][1]

        cursor.execute(
            f"""
            SELECT kodehotel, tglmasuk FROM reservasi_hotel
            WHERE kodepasien = '{kodepasien}';
            """
        )

        res = cursor.fetchall()
        res.sort(key= lambda x:x[1], reverse=True)

        kodehotel = res[0][0]

        # ambil paket makan dari kode hotel X
        cursor.execute(
            f"""
            SELECT kodehotel, kodepaket, nama FROM paket_makan
            WHERE kodehotel = '{kodehotel}';
            """
        )
        list_paket = cursor.fetchall()

        # Proses hitung id transaksi makan baru
        id_transaksihotel = list_id[0][0]
        cursor.execute(
            f"""
            SELECT MAX(idtransaksimakan) FROM transaksi_makan
            WHERE idtransaksi = '{id_transaksihotel}';
            """
        )

        latest_id_TM = cursor.fetchall()[0][0]
        newTM = "TM" + str(int(latest_id_TM[2:]) + 1).zfill(4)

        context = {
            'list_id': list_id,
            'list_paket': list_paket,
            'newTM': newTM,
            'kode_hotel': kodehotel,
            'has_reservation': True
        }

        conn.close()


        return render(request, 'buat-transaksi-makan.html', context)
        

    return render(request, 'buat-transaksi-makan.html')



@csrf_exempt
@require_login
def tambah_transaksi_makan(request):
    if request.method == "POST":

        conn = open_connection()
        cursor = conn.cursor()
        cursor.execute("SET search_path to siruco;")

        id_transaksimakan = request.POST.get('id_transaksimakan')
        id_transaksihotel = request.POST.get('id_transaksi')
        kodehotel = request.POST.get('kodehotel')
        kodepaket = request.POST.get('kodepaket')

        # Cek udah ada idTM yang sama apa belom
        cursor.execute(
            f"""
            SELECT * FROM transaksi_makan
            WHERE idtransaksi = '{id_transaksihotel}'
            AND idtransaksimakan = '{id_transaksimakan}';
            """
        )
        result = cursor.fetchall()
        isExist = True if len(result) > 0 else False

        if not isExist:
            cursor.execute(
                f"""
                INSERT INTO transaksi_makan VALUES(
                    '{id_transaksihotel}', '{id_transaksimakan}', 0
                );
                """
            )
            conn.commit()

        # INSERT ke daftar pesan dengan idpesanan MAX + 1
        cursor.execute("SELECT MAX(id_pesanan) from daftar_pesan;")
        id_pesanan_baru = int(cursor.fetchall()[0][0]) + 1

        cursor.execute(
            f"""
            INSERT INTO DAFTAR_PESAN VALUES(
                '{id_transaksimakan}', {id_pesanan_baru},
                '{id_transaksihotel}', '{kodehotel}',
                '{kodepaket}'
            );
            """
        )

        conn.commit()
        conn.close()

        return HttpResponse("Succeed.")

@csrf_exempt
@require_login
def data_change_handler(request):
    if request.method == "POST":
        conn = open_connection()
        cursor = conn.cursor()
        cursor.execute("SET search_path to siruco;")

        idtransaksihotel = request.POST.get("idtransaksihotel")

        # cari kode hotel dari id transaksi baru
        cursor.execute(
            f"""
            SELECT kodehotel FROM
            (SELECT kodepasien FROM transaksi_hotel where idtransaksi = '{idtransaksihotel}') as A
            JOIN
            (SELECT kodepasien, kodehotel from reservasi_hotel) as B 
            ON A.kodepasien = B.kodepasien;
            """
        )
        kodehotel = cursor.fetchall()[0][0]

        # Ambil Data List Paket Makan
        cursor.execute(
            f"""
            SELECT kodehotel, kodepaket, nama FROM paket_makan
            WHERE kodehotel = '{kodehotel}'
            """
        )
        list_paket = []
        list_paket.extend(cursor.fetchall())

        # Ambil id transaksi makan baru + 1
        cursor.execute(
            f"""
            SELECT MAX(idtransaksimakan) FROM transaksi_makan
            WHERE idtransaksi = '{idtransaksihotel}';
            """
        )

        old_TM = cursor.fetchall()[0][0]
        new_TM = "TM" + str(int(old_TM[2:]) + 1).zfill(4)

        context = {
            'id_transaksimakan': new_TM,
            'kodehotel': kodehotel,
            'list_paket': list_paket
        }

        return JsonResponse(context)


# --------- READ VIEW
@require_login
def read_transaksi_makan(request):
    connection = open_connection()
    cursor = open_connection().cursor()
    role = request.session.get('peran')
    transaksi = []

    if (role == 'admin satgas'):
        cursor.execute('SELECT * FROM siruco.transaksi_makan;')
        transaksi = cursor.fetchall()
        transaksi.sort(key=lambda x:x[0])
    
    elif (role == 'pengguna publik'):
        username = request.session['username']
        transaksi = cursor.execute(
            f'''
                SELECT * FROM siruco.transaksi_makan AS tm, 
                (select p.nik, th.idtransaksi FROM
                siruco.pasien p, siruco.transaksi_hotel th
                WHERE p.nik = th.kodepasien AND p.idpendaftar = '{username}') AS b
                WHERE tm.idtransaksi = b.idtransaksi;
            '''
        )
        transaksi = cursor.fetchall()
        transaksi.sort(key=lambda x:x[0])
    
    context = {
        'transaksi': transaksi,
        'role': role
    }

    connection.close()

    return render(request, 'list-transaksi-makan.html', context)


@require_login
def detail_transaksi_makan(request, id_transaksi, id_transaksimakan):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute(
        f'''
        SELECT pm.kodehotel, a.id_pesanan, pm.nama, pm.harga 
        FROM siruco.paket_makan AS pm,
        (SELECT * FROM siruco.daftar_pesan
        WHERE idtransaksimakan = '{id_transaksimakan}' AND
        id_transaksi = '{id_transaksi}') AS a
        WHERE a.kodehotel = pm.kodehotel AND
        a.kodepaket = pm.kodepaket;
        '''
    )

    detail = cursor.fetchall()
    context = {
        "detail":detail,
        'id_transaksi':id_transaksi,
        'id_transaksimakan': id_transaksimakan
    }

    conn.close()

    return render(request, 'detail-transaksi-makan.html', context)
    

# --------- UPDATE VIEW
@require_login
def update_transaksi_makan(request, id_transaksi, id_transaksimakan):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute("SET search_path to siruco;")
    context = {}

    cursor.execute(
        f"""
        SELECT statusbayar FROM
        (SELECT * FROM transaksi_makan
        WHERE idtransaksi = '{id_transaksi}' AND
        idtransaksimakan = '{id_transaksimakan}') as A
        JOIN transaksi_hotel as th ON a.idtransaksi = th.idtransaksi
        WHERE statusbayar != 'Lunas';
        """
    )

    
    updatable = True if len(cursor.fetchall()) > 0 else False

    if not updatable:
        context['updatable'] = False
        return render(request, 'update-transaksi-makan.html', context)

    # Fetching kode pesanan yang telah dibuat beserta namanya 
    cursor.execute(
        f"""
        SELECT * FROM
        (SELECT * FROM daftar_pesan
        WHERE id_transaksi = '{id_transaksi}'
        AND idtransaksimakan = '{id_transaksimakan}') AS A
        JOIN paket_makan pm ON A.kodehotel = pm.kodehotel
        AND A.kodepaket = pm.kodepaket;
        """
    )
    ordered = cursor.fetchall()


    # Fetching semua paket makan di hotel X
    cursor.execute(
        f"""
        SELECT * FROM paket_makan WHERE kodehotel = '{ordered[0][3]}';
        """
    )
    list_paket = cursor.fetchall()

    context = {
        'id_transaksi': id_transaksi,
        'id_transaksimakan':id_transaksimakan,
        'updatable': True,
        'ordered': ordered,
        'list_paket': list_paket,
        'kode_hotel': ordered[0][3]
    }

    conn.close()

    return render(request, 'update-transaksi-makan.html', context)

@csrf_exempt
@require_login
def tambah_pesanan(request):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute('SET search_path to siruco;')
    if request.method == 'POST':
        kodepaket = request.POST.get('kodepaket')
        kodehotel = request.POST.get('kodehotel')
        id_transaksi = request.POST.get('id_transaksi')
        id_transaksimakan = request.POST.get('id_transaksimakan')
        
        cursor.execute('SELECT MAX(id_pesanan) FROM daftar_pesan;')
        id_pesanan = int(cursor.fetchall()[0][0]) + 1

        cursor.execute(
            f"""
            INSERT INTO daftar_pesan VALUES ('{id_transaksimakan}',{id_pesanan},
            '{id_transaksi}','{kodehotel}', '{kodepaket}');
            """
        )
        conn.commit()
        conn.close()

        return HttpResponse("Succeed.")



# ------------ DELETE View
@csrf_exempt
@require_login
def hapus_transaksi_makan(request):
    if request.session.get('peran') == 'admin satgas':
        if request.method == "POST":
            conn = open_connection()
            cursor = conn.cursor()
            cursor.execute("SET search_path to siruco;")

            id_transaksihotel = request.POST.get('id_transaksihotel')
            id_transaksimakan = request.POST.get('id_transaksimakan')

            # Cek udah lunas apa belom
            cursor.execute(
                f"""
                SELECT statusbayar from transaksi_hotel
                WHERE idtransaksi = '{id_transaksihotel}';
                """
            )

            notPaid = True if cursor.fetchall()[0][0].lower() != "lunas" else False

            if notPaid:
                cursor.execute(
                    f"""
                    DELETE FROM transaksi_makan WHERE
                    idtransaksi = '{id_transaksihotel}'
                    AND idtransaksimakan = '{id_transaksimakan}';
                    """
                )

                conn.commit()
                conn.close()
                return HttpResponse("Succeed.")


            conn.close()
            return HttpResponseNotAllowed()
