$('.btn-delete').click(function() {
    let id_transaksihotel = $(this).val().split("-")[0];
    let id_transaksimakan = $(this).val().split("-")[1];
    let confirmation = confirm("Apakah kamu yakin akan delete transaksi makan ini?");

    if (confirmation) {
        $.ajax({
            url:'/transaksi-makan/hapus-transaksi/',
            method:"POST",
            data:{
                'id_transaksihotel': id_transaksihotel,
                'id_transaksimakan': id_transaksimakan
            },
            success: (e) => {
                console.log(e);
                alert("Transaksi makan berhasil dihapus.")
                location.reload();
            }, error: (e) => {
                alert("Transaksi tidak dapat dihapus!");
            }
        })
    }
})