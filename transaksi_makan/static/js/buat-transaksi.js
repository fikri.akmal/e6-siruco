var btn_delete = $('.button-delete').parent().clone();

$('#btn-tambah').click(function() {
    $('.section-list-paket').append(btn_delete.clone());
})

$('#btn-simpan').click(function() {
    let listpaket = $('.section-list-paket').children();
    if (listpaket.length > 0) {
        let dropdown_list = document.getElementsByClassName('container-dropdown');
        for (let i=0; i<dropdown_list.length; i++) {

            let data = dropdown_list[i].children[0].value.split("-");
            let kodehotel = data[0];
            let kodepaket = data[1];
            let id_transaksimakan = $('#input_idTM').val();
            let id_transaksihotel = $('#idtransaksihotel').val()

            $.ajax({
                url:'/transaksi-makan/tambah-transaksi-makan/',
                type:"POST",
                async:false,
                data: {
                    'id_transaksimakan': id_transaksimakan,
                    'id_transaksi': id_transaksihotel,
                    'kodehotel': kodehotel,
                    'kodepaket': kodepaket
                },
                success: (e) => {
                    console.log("berhasil menambahkan");
                }
            })
        }

        alert('Transaksi Makan berhasil ditambahkan');
        window.location.pathname = ('/transaksi-makan/list-transaksi-makan')
    
    } else if (listpaket.length == 0) {
        alert('Minimal tambahan pesanan adalah 1!');
    }
})

const handleOnChange = (elem) => {
    $('.section-list-paket').empty();

    $.ajax({
        url:'/transaksi-makan/data-change-handler/',
        method:"POST",
        data:{
            idtransaksihotel: elem.value
        }, success:(e) => {
            var newDropdown = generateDropdown(e.list_paket);
            $('.section-list-paket').append(newDropdown);
            btn_delete = $('.section-list-paket .container-dropdown').last().clone();

            $('#kodehotel').html("Kode hotel: " + e.kodehotel);
            $('#input_kodehotel').val(e.kodehotel);

            $('#idtransaksimakan').html("Id Transaksi Makan: " + e.id_transaksimakan);
            $('#input_idTM').val(e.id_transaksimakan);
        }, error: (e) => {
            alert("error");
        }
    });
}


const generateDropdown = (data) => {
    let element = `<div class="container-dropdown">
      <select name="paket" id="paket">
        ${data.map(x => 
            `<option value="${x[0]}-${x[1]}">
            ${x[1]} - ${x[2]}
            </option>`
        )}
      </select>
      <button class="button-delete" onclick="$(this).parent().remove();">
        Delete
      </button>
    </div>`

    return element;
}