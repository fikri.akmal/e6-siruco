var btn_delete = $('.button-delete').parent().clone();
var link_split = window.location.href.split('/')
var transaksi_hotel = link_split[5];
var transaksi_makan = link_split[6];

// Tambah button
$('.button-tambah').click(function() {
    $('.section-list-paket').append(btn_delete.clone());
})

// Simpan pesanan
$('#button-simpan').click(function() {
    let listpaket = $('.section-list-paket').children();
    if (listpaket.length > 0) {
        console.log("simpan");
        let dropdown_list = document.getElementsByClassName('container-dropdown');
        for (let i = 0; i < dropdown_list.length; i++) {
            let data = dropdown_list[i].children[0].value.split("-");
            let kodehotel = data[0];
            let kodepaket = data[1];
            $.ajax({
                url: '/transaksi-makan/ajax-tambah-pesanan/',
                type:"POST",
                async:false,
                data: {
                    'id_transaksimakan': transaksi_makan,
                    'id_transaksi': transaksi_hotel,
                    'kodehotel': kodehotel,
                    'kodepaket': kodepaket
                },
                success: (e) => {}
            })
        }

        alert('Paket berhasil ditambahkan');
        window.location.pathname = ('/transaksi-makan/list-transaksi-makan')
    
    } else if (listpaket.length == 0) {
        console.log('tidak bisa simpan')
        alert('Minimal tambahan pesanan adalah 1!');
    }
});
