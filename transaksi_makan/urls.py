from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('buat-transaksi-makan/', create_transaksi_makan, name='buat-transaksi-makan'),
    path('list-transaksi-makan/', read_transaksi_makan, name="list-transaksi-makan" ),
    path(
        'detail-transaksi/<str:id_transaksi>/<str:id_transaksimakan>/',
        detail_transaksi_makan,
        name='detail-transaksi-makan'
    ),
    path(
        'update-transaksi/<str:id_transaksi>/<str:id_transaksimakan>/',
        update_transaksi_makan,
        name='update-transaksi-makan'
    ),
    path('ajax-tambah-pesanan/', tambah_pesanan, name='tambah-pesanan'),
    path('hapus-transaksi/', hapus_transaksi_makan, name='hapus-transaksi'),
    path('data-change-handler/', data_change_handler, name='data-change-handler'),
    path('tambah-transaksi-makan/', tambah_transaksi_makan, name='tambah-transaksi-makan')
]
