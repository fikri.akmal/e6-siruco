from functools import wraps
from django.shortcuts import redirect

'''
Decorator untuk memvalidasi user login sebelum mengakses sebuah halaman
How to use: 
Gunakan @require_login sebelum pendefinisian fungsi yang kamu buat.
'''

def require_login(func):
    def passed_func(request, *args, **kwargs):
        if request.session.get('username', False):
            return func(request, *args, **kwargs)
        
        return redirect("authentication:index")

    return passed_func
