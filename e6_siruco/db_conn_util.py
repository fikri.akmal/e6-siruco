from urllib.parse import urlparse
import psycopg2
import os

# Method for opening connection to database

# You can use this for local configuration too by adding 
# environment variable 'DATABASE_URL' with the value of
# your local database URL to your machine

def open_connection() -> psycopg2.extensions.connection:

    url = os.getenv('DATABASE_URL')
    res = urlparse(url)

    database = res.path[1:]
    host = res.hostname
    username =  res.username
    password = res.password
    port = res.port

    connection = psycopg2.connect(
        user = username,
        password = password,
        host = host,
        database = database,
        port = port
    )

    return connection