"""e6_siruco URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('', include('hanif.urls')),
    path('authentication/', include('authentication.urls')),
    path('transaksi-makan/', include('transaksi_makan.urls')),
    path('paket-makan/', include('paket_makan.urls')),
    path('hotel/', include('hotel.urls')),
    path('jadwal-dokter/', include('jadwal_dokter.urls')),
    path('daftar-pasien/', include('daftar_pasien.urls')),
    path('appointment/', include('appointment.urls')),
    path('faskes/', include('faskes.urls')),
    path('jadwal_faskes/', include('jadwal_faskes.urls')),
    path('rumahsakit/', include('rumahsakit.urls')),
    path('reservasiRS/', include('reservasiRS.urls')),
    path('transaksirs/', include('transaksirs.urls')),
    path('ruangan-rs/', include('ruangan_rs.urls')),
]
