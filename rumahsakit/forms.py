from django import forms
from django.db import connection


class CreateRumahSakit(forms.Form):
    faskes_list = []
    faskes = forms.ChoiceField(choices=faskes_list, required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT KODE,KODE FROM SIRUCO.FASKES EXCEPT SELECT KODE_FASKES, KODE_FASKES FROM SIRUCO.RUMAH_SAKIT;")
            self.fields['faskes'].choices = cursor.fetchall()


class UpdateRumahSakit(forms.Form):
    faskes_list = []
    faskes = forms.CharField(label=("Faskes"), required=True, max_length=3)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['faskes'].widget.attrs['readonly'] = True
