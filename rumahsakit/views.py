from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from e6_siruco.auth_decorator import require_login
from django.db import connection


@require_login
def create_RS(request):
    if request.session.get('peran', False) == 'admin satgas':
        form = CreateRumahSakit()
        if request.method == "POST":
            kode_faskes = request.POST['faskes']
            isRujukan = ''
            try:
                if request.POST['isRujukan'] == '1':
                    isRujukan = '1'
                else:
                    isRujukan = '0'
            except:
                isRujukan = '0'

            data_baru = [kode_faskes, isRujukan]
            with connection.cursor() as cursor:
                cursor.execute(
                    'INSERT INTO SIRUCO.RUMAH_SAKIT VALUES (%s, %s)', data_baru)
            return redirect('rumahsakit:read')
        return render(request, 'buatrs.html', {'form': form, 'peran': request.session['peran']})


@require_login
def readRS(request):
    if request.session.get('peran', False) == 'admin satgas':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.RUMAH_SAKIT")
            seluruh = dictfetchall(cursor)
        context = {'rss': seluruh}
        return render(request, 'read-rumah-sakit.html', context)


@require_login
def updateRS(request):
    if request.session.get('peran', False) == 'admin satgas':
        isRujukan = ''
        if request.method == 'GET':
            if request.GET.get('kode') is not None:
                kode = request.GET.get('kode')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.RUMAH_SAKIT where kode_faskes=%s LIMIT 1;', [kode])
                    data = dictfetchall(cursor)
                data_faskes = {}
                data_faskes['faskes'] = data[0]['kode_faskes']
                isRujukan = data[0]['isrujukan']
                form = UpdateRumahSakit(initial=data_faskes)
        else:

            kode = request.POST['faskes']
            tipe = ''
            try:
                if request.POST['isRujukan'] == '1':
                    tipe = '1'
                else:
                    tipe = '0'
            except:
                tipe = '0'
            data_baru = [tipe, kode]
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.RUMAH_SAKIT SET isrujukan = %s WHERE kode_faskes = %s", data_baru)
            return redirect('rumahsakit:read')
        return render(request, 'update-rumah-sakit.html', {'form': form, 'isRujukan': isRujukan})


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
