from django.urls import path

from .views import create_RS, readRS, updateRS
app_name = 'rumahsakit'

urlpatterns = [
    path('create/', create_RS, name='create'),
    path('read/', readRS, name='read'),
    path('update/', updateRS, name='update'),
]
