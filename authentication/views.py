from django.shortcuts import render, redirect
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from django.contrib import messages
import traceback

# Create your views here.
def index(request):
    return render(request, 'authentication/index.html')

def login(request):
    if request.session.get("username", False):
        return redirect("authentication:index")

    if request.method == "POST":
        with connection.cursor() as cursor:

            # ini buat execute query database
            cursor.execute(f"""
                SELECT * FROM AKUN_PENGGUNA
                WHERE username='{request.POST["username"]}'
                AND password='{request.POST["password"]}'
                """)

            row = cursor.fetchall() # ini dibalikin dalem bentuk multi-dimensional array
            if (len(row) != 0): # Berhasil login

                # contohnya buat set session.username menjadi row pertama (index ke 0) kolom pertama (index ke 0)
                # dalem kasus gw, kolom username adalah kolom paling awal di table PENGGUNA
                request.session["username"] = row[0][0] 
                request.session["password"] = row[0][1] 
                request.session["peran"] = row[0][2] 
                return redirect("main:home")

            else: # Gagal login
                messages.add_message(request, messages.WARNING, "Maaf, username atau password salah.")

    return render(request, "authentication/index.html")

def register(request):
    if request.session.get("username", False):
        return redirect("main:home")

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT FROM AKUN_PENGGUNA WHERE username = '{request.POST["username"]}'
                """)

                if (len(cursor.fetchall()) == 0):
                    cursor.execute(f"""
                        INSERT INTO AKUN_PENGGUNA (username, password, peran) VALUES
                        ('{request.POST["username"]}', '{request.POST["password"]}', '{request.POST["peran"]}')
                    """)

                peran = {request.POST["peran"]}
                if (peran == {'admin sistem'}):
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES
                        ('{request.POST["username"]}')
                    """)

                elif (peran == {'pengguna publik'}):
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA_PUBLIK (username, nik, nama, status, peran, nohp) VALUES
                        ('{request.POST["username"]}', '{request.POST["nik"]}', '{request.POST["nama"]}', 'AKTIF', '{request.POST["peran2"]}', '{request.POST["no_hp"]}')
                    """)

                elif (peran == {'dokter'}):
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES
                        ('{request.POST["username"]}')
                    """)
                    cursor.execute(f"""
                        INSERT INTO DOKTER (username, nostr, nama, nohp, gelardepan, gelarbelakang) VALUES
                        ('{request.POST["username"]}', '{request.POST["no-str"]}', '{request.POST["nama"]}', '{request.POST["no-hp"]}', 
                        '{request.POST["gelar-depan"]}', '{request.POST["gelar-belakang"]}')
                    """)

                elif (peran == {'admin satgas'}):
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES
                        ('{request.POST["username"]}')
                    """)
                    cursor.execute(f"""
                        INSERT INTO admin_satgas (username, idfaskes) VALUES
                        ('{request.POST["username"]}', '{request.POST["kode"]}')
                    """)


                messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
                messages.add_message(request, messages.SUCCESS, f"{request.POST['username']}")

                return render(request, "authentication/register.html")
            
            # Ada username yang sama, handle disini
            except InternalError:
                messages.add_message(request, messages.WARNING, f'''Password Anda belum memenuhi syarat, silahkan 
                pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka''')
            except IntegrityError:
                messages.add_message(request, messages.WARNING, f"Username {request.POST['username']} sudah ada")
            except:
                traceback.print_exc()
                print(request.POST.get("kode", False))
                messages.add_message(request, messages.WARNING, f"Ada gangguan server internal, mohon coba lagi")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT kode FROM FASKES
        """)
        row = cursor.fetchall()
        context = []
        for i in range(len(row)):
            context.append(row[i][0])

    return render(request, "authentication/register.html", {'context':context})


def logout(request):
    # Hilangkan semua dari session
    request.session.pop("username", None)
    request.session.pop("password", None)
    request.session.pop("peran", None)

    return redirect("authentication:index")
