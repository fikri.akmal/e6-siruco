function check() {
    if(document.getElementById('pengguna-publik').checked) {
        document.getElementById('form-admin-satgas').style.display = "none";
        document.getElementById('form-tambahan').innerHTML = 
        `<div id="form-pengguna-publik">
          <div class="pb-1">NIK</div>
          <div class="form-group">
            <input type="text" name="nik" class="form-width form-control" aria-describedby="nik" placeholder="nik" required>
          </div>
          
          <div class="pb-1">Nama</div>
          <div class="form-group">
            <input type="text" name="nama" class="form-width form-control" placeholder="Nama" required>
          </div>

          <div class="pb-1">Nomor HP</div>
          <div class="form-group">
            <input type="text" name="no_hp" class="form-width form-control" placeholder="Nomor HP" required>
          </div>

          <p>Peran</p>
          <input type="radio" id="calon-pasien" name="peran2" value="calon pasien">
          <label for="calon-pasien">Calon Pasien</label>
          <input type="radio" id="penanggung-jawab" name="peran2" value="penanggung jawab">
          <label for="penanggung-jawab">Penanggung Jawab</label><br> 
        </div>`
    } else if (document.getElementById('dokter').checked) {
        document.getElementById('form-admin-satgas').style.display = "none";
        document.getElementById('form-tambahan').innerHTML = 
        `<div id="form-dokter">
        <div class="pb-1">No STR</div>
        <div class="form-group">
          <input type="text" name="no-str" class="form-width form-control" aria-describedby="no str" placeholder="No STR" required>
        </div>
        
        <div class="pb-1">Nama</div>
        <div class="form-group">
          <input type="text" name="nama" class="form-width form-control" placeholder="Nama" required>
        </div>

        <div class="pb-1">Nomor HP</div>
        <div class="form-group">
          <input type="text" name="no-hp" class="form-width form-control" placeholder="Nomor HP" required>
        </div>

        <div class="pb-1">Gelar Depan</div>
        <div class="form-group">
          <input type="text" name="gelar-depan" class="form-width form-control" placeholder="Gelar Depan" required>
        </div>

        <div class="pb-1">Gelar Belakang</div>
        <div class="form-group">
          <input type="text" name="gelar-belakang" class="form-width form-control" placeholder="Gelar Belakang" required>
        </div>
      </div>`
    } else if (document.getElementById('admin').checked) {
        document.getElementById('form-tambahan').innerHTML = ''
        document.getElementById('form-admin-satgas').style.display = "none";
    } else if (document.getElementById('admin-satgas').checked) {
        document.getElementById('form-tambahan').innerHTML = ''
        document.getElementById('form-admin-satgas').style.display = "block";
      }
}