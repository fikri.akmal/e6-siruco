from django.shortcuts import render
from django.db import connection
import traceback


def home(request):
    with connection.cursor() as cursor:
        try:
            if request.session.get("peran") == 'dokter':
                cursor.execute(f"""
                    SELECT * FROM DOKTER
                    WHERE username='{request.session.get("username")}'
                """)
                info_peran = cursor.fetchall()
            elif request.session.get("peran") == 'pengguna publik':
                cursor.execute(f"""
                    SELECT * FROM pengguna_publik
                    WHERE username='{request.session.get("username")}'
                """)
                info_peran = cursor.fetchall()
                print("masuk pa eko")
                print(info_peran)
            return render(request, 'main/home.html', {"info_peran":info_peran})
        except:
            traceback.print_exc()
    return render(request, 'main/home.html')
