from os import name
from django.contrib import admin
from django.urls import path
from .views import *

app_name = 'hotel'

urlpatterns = [
    path('buat-hotel/', make_hotel, name='buat-hotel'),
    path('list-hotel/', list_hotel, name='list-hotel'),
    path('ubah-hotel/<str:kodehotel>/', update_hotel, name='ubah-hotel')
]
