from decimal import Context
from django.http.response import HttpResponse
from e6_siruco.db_conn_util import open_connection
from django.views.decorators.csrf import csrf_exempt
from e6_siruco.auth_decorator import require_login
from django.shortcuts import redirect, render

# Create your views here.


# ----------- CREATE View
@csrf_exempt
@require_login
def make_hotel(request):
    if request.session.get('peran', False) == 'admin sistem':

        conn = open_connection()
        cursor = conn.cursor()
        cursor.execute("SET search_path to siruco;")
        context = {}

        if request.method == "POST":
            kodehotel = request.POST.get('kodehotel')
            namahotel = request.POST.get('namahotel')
            rujukan = 1 if request.POST.get('rujukan') == 'on' else 0
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabupatenkota = request.POST.get('kabupatenkota')
            provinsi = request.POST.get('provinsi')

            cursor.execute(
                f"""
                INSERT INTO HOTEL VALUES (
                    '{kodehotel}', '{namahotel}', '{rujukan}', 
                    '{jalan}', '{kelurahan}', '{kecamatan}', 
                    '{kabupatenkota}', '{provinsi}'
                );
                """
            )

            conn.commit()
            conn.close()


            return redirect("hotel:list-hotel")


        # Ambil kode hotel terakhir
        cursor.execute("SELECT MAX(kode) FROM hotel;")

        currentMax = cursor.fetchall()[0][0]
        maxNumber = int(currentMax[1:])
        newKode = "H" + str(maxNumber + 1)

        context = {
            'newKode': newKode
        }

        return render(request, 'buat-hotel.html', context)


# ------------- READ View
@require_login
def list_hotel(request):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute("SET search_path to siruco;")
    cursor.execute("SELECT * FROM hotel;")
    list_hotel = cursor.fetchall()

    context = {
        'list_hotel': list_hotel
    }

    if request.session.get('peran') == 'admin sistem':
        context['isAdminSistem'] = True
    else:
        context['isAdminSistem'] = False

    conn.close()

    return render(request, 'list-hotel.html', context)


# --------------- UPDATE View
@csrf_exempt
@require_login
def update_hotel(request, kodehotel):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute("SET search_path to siruco;")
    if request.session.get('peran') == 'admin sistem':
        if request.method == "POST":
            kode_hotel = request.POST.get('kodehotel')
            namahotel = request.POST.get('namahotel')
            rujukan = 1 if request.POST.get('rujukan') == 'on' else 0
            jalan = request.POST.get('jalan')
            kelurahan = request.POST.get('kelurahan')
            kecamatan = request.POST.get('kecamatan')
            kabupatenkota = request.POST.get('kabupatenkota')
            provinsi = request.POST.get('provinsi')

            cursor.execute(
                f"""
                UPDATE hotel set nama = '{namahotel}', isrujukan = '{rujukan}',
                jalan = '{jalan}', kelurahan = '{kelurahan}', kecamatan = '{kecamatan}',
                kabkot = '{kabupatenkota}', prov = '{provinsi}' 
                WHERE kode = '{kode_hotel}';
                """
            )

            conn.commit()
            conn.close()

            return redirect("hotel:list-hotel")

        

        cursor.execute(
            f"""
            SELECT * FROM hotel WHERE kode = '{kodehotel}';
            """
        )

        res = cursor.fetchall()


        context = {
            'kodehotel': kodehotel,
            'data':res
        }

        conn.close()
        return render(request, 'update-hotel.html', context)
