from django.urls import path

from . import views

app_name = 'appointment'

urlpatterns = [
    path('', views.index, name='index'),
    path('buat-appointment/', views.buat_appointment, name='buat_appointment'),
    path('form-appointment/', views.form_appointment, name='form_appointment'),
    path('form-submit/', views.submit, name='submit'),
    path('delete-jadwal/', views.delete, name='delete'),
    path('update/', views.update, name='update'),
    path('update-jadwal/', views.update_form, name='update-form')
]
