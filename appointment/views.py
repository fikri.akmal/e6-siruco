import appointment
from django.shortcuts import render, redirect
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from django.contrib import messages
from datetime import datetime
import traceback

# Create your views here.
def index(request):
    with connection.cursor() as cursor:
        try:
            if request.session["peran"] == 'pengguna publik':
                print("masuk1")
                print(request.session["peran"])
                cursor.execute(f"""
                    SELECT * FROM MEMERIKSA
                    WHERE nik_pasien IN (SELECT nik FROM PASIEN WHERE idpendaftar='{request.session.get('username')}')
                """)
                memeriksa = cursor.fetchall()
                for i in range(len(memeriksa)):
                    temp = list(memeriksa[i])
                    if temp[4] == 'pertama':
                        temp[4] = 'Shift 1'
                    elif temp[4] == 'kedua':
                        temp[4] = 'Shift 2'
                    elif temp[4] == 'ketiga':
                        temp[4] = 'Shift 3'
                    elif temp[4] == 'keempat':
                        temp[4] = 'Shift 4'
                    temp[5] = datetime.strftime(temp[5], '%d-%m-%Y')
                    memeriksa[i] = temp
            elif request.session["peran"] == 'admin satgas':
                print("masuk")
                cursor.execute(f"""
                    SELECT * FROM MEMERIKSA
                """)
                memeriksa = cursor.fetchall()
                for i in range(len(memeriksa)):
                    temp = list(memeriksa[i])
                    if temp[4] == 'pertama':
                        temp[4] = 'Shift 1'
                    elif temp[4] == 'kedua':
                        temp[4] = 'Shift 2'
                    elif temp[4] == 'ketiga':
                        temp[4] = 'Shift 3'
                    elif temp[4] == 'keempat':
                        temp[4] = 'Shift 4'
                    temp[5] = datetime.strftime(temp[5], '%d-%m-%Y')
                    memeriksa[i] = temp
            elif request.session["peran"] == 'dokter':
                print("masuk")
                cursor.execute(f"""
                    SELECT * FROM MEMERIKSA
                    WHERE username_dokter='{request.session["username"]}'
                """)
                memeriksa = cursor.fetchall()
                for i in range(len(memeriksa)):
                    temp = list(memeriksa[i])
                    if temp[4] == 'pertama':
                        temp[4] = 'Shift 1'
                    elif temp[4] == 'kedua':
                        temp[4] = 'Shift 2'
                    elif temp[4] == 'ketiga':
                        temp[4] = 'Shift 3'
                    elif temp[4] == 'keempat':
                        temp[4] = 'Shift 4'
                    temp[5] = datetime.strftime(temp[5], '%d-%m-%Y')
                    memeriksa[i] = temp
            return render(request, 'appointment/index.html', {"memeriksa":memeriksa})
        except:
            traceback.print_exc()
    return render(request, 'appointment/index.html')

def buat_appointment(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM JADWAL_DOKTER
            """)

            jadwal_dokter = cursor.fetchall()

            for i in range(len(jadwal_dokter)):
                temp = list(jadwal_dokter[i])
                if temp[3] == 'pertama':
                    temp[3] = 'Shift 1'
                elif temp[3] == 'kedua':
                    temp[3] = 'Shift 2'
                elif temp[3] == 'ketiga':
                    temp[3] = 'Shift 3'
                elif temp[3] == 'keempat':
                    temp[3] = 'Shift 4'
                temp[4] = datetime.strftime(temp[4], '%d-%m-%Y')
                jadwal_dokter[i] = temp
        except:
            traceback.print_exc()
    return render(request, 'appointment/create.html', {'jadwal_dokter':jadwal_dokter})

def form_appointment(request):
    with connection.cursor() as cursor:
        try:
            shift = ''
            if request.POST['shift'] == 'Shift 1':
                shift = 'pertama'
            elif request.POST['shift'] == 'Shift 2':
                shift = 'kedua'
            elif request.POST['shift'] == 'Shift 3':
                shift = 'ketiga'
            elif request.POST['shift'] == 'Shift 4':
                shift = 'keempat'

            if request.session.get("peran") == 'pengguna publik':
                cursor.execute(f"""
                    SELECT nik FROM PASIEN
                    WHERE idpendaftar='{request.session['username']}'
                """)
                nik_pasien = cursor.fetchall()
            elif request.session.get("peran") == 'admin satgas':
                cursor.execute(f"""
                    SELECT nik FROM PASIEN
                """)
                nik_pasien = cursor.fetchall()

            cursor.execute(f"""
                SELECT * FROM JADWAL_DOKTER
                WHERE nostr='{request.POST['nostr']}' AND username='{request.POST['username']}' AND kode_faskes='{request.POST['faskes']}'
                AND shift='{shift}' AND tanggal='{request.POST['tanggal']}'
            """)
            jadwal_dokter = cursor.fetchall()

            return render(request, 'appointment/form.html', {'nik_pasien':nik_pasien, 'jadwal_dokter':jadwal_dokter})

        except:
            traceback.print_exc()
    return render(request, 'appointment/form.html')

def submit(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                INSERT INTO MEMERIKSA (nik_pasien, nostr, username_dokter, kode_faskes, praktek_shift, praktek_tgl) VALUES
                ('{request.POST['nik']}', '{request.POST['nostr']}', '{request.POST['username']}', 
                '{request.POST['faskes']}', '{request.POST['shift']}', '{request.POST['tanggal']}')
            """)
            return redirect('appointment:index')
        except InternalError:
            shift = ''
            if request.POST['shift'] == 'pertama':
                shift = 'Shift 1'
            elif request.POST['shift'] == 'kedua':
                shift = 'Shift 2'
            elif request.POST['shift'] == 'ketiga':
                shift = 'Shift 3'
            elif request.POST['shift'] == 'keempat':
                shift = 'Shift 4'
            tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
            tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"""Silahkan pilih shift dan tanggal lainnya,
                karena shift dan tanggal yang dipilih sudah penuh""")
            return buat_appointment(request)

def delete(request):
    if request.sesssion.get('peran') != 'admin satgas':
        return redirect('main:home')
    else:
        with connection.cursor() as cursor:
            try:
                shift = ''
                if request.POST['shift'] == 'Shift 1':
                    shift = 'pertama'
                elif request.POST['shift'] == 'Shift 2':
                    shift = 'kedua'
                elif request.POST['shift'] == 'Shift 3':
                    shift = 'ketiga'
                elif request.POST['shift'] == 'Shift 4':
                    shift = 'keempat'
                cursor.execute(f"""
                    DELETE FROM MEMERIKSA
                    WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                    AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{shift}'
                    AND praktek_tgl='{request.POST['tanggal']}'
                """)
            except:
                traceback.print_exc()
                messages.add_message(request, messages.WARNING, f"""Ada gangguan server, harap coba lagi""")
        return redirect('appointment:index')

def update(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            try:
                tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
                tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
                cursor.execute(f"""
                    UPDATE MEMERIKSA
                    SET rekomendasi='{request.POST['rekomendasi']}'
                    WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                    AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{request.POST['shift']}'
                    AND praktek_tgl='{tanggal}'
                """)
            except:
                traceback.print_exc()
    
    return redirect('appointment:index')


def update_form(request):
    with connection.cursor() as cursor:
        try:
            shift = ''
            if request.POST['shift'] == 'Shift 1':
                shift = 'pertama'
            elif request.POST['shift'] == 'Shift 2':
                shift = 'kedua'
            elif request.POST['shift'] == 'Shift 3':
                shift = 'ketiga'
            elif request.POST['shift'] == 'Shift 4':
                shift = 'keempat'
            cursor.execute(f"""
                SELECT * FROM MEMERIKSA
                WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{shift}'
                AND praktek_tgl='{request.POST['tanggal']}'
            """)
            memeriksa = cursor.fetchall()
        except:
            traceback.print_exc()
    
    return render(request, 'appointment/update-form.html', {'memeriksa':memeriksa})