from django.shortcuts import render, redirect
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from django.contrib import messages
from datetime import datetime
import traceback

# Create your views here.
def index(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM JADWAL_DOKTER
            """)

            var = cursor.fetchall()
            for i in range(len(var)):
                temp = list(var[i])
                if temp[3] == 'pertama':
                    temp[3] = 'Shift 1'
                elif temp[3] == 'kedua':
                    temp[3] = 'Shift 2'
                elif temp[3] == 'ketiga':
                    temp[3] = 'Shift 3'
                elif temp[3] == 'keempat':
                    temp[3] = 'Shift 4'
                temp[4] = datetime.strftime(temp[4], '%d-%m-%Y')
                var[i] = temp

            cursor.execute(f"""
                SELECT * FROM JADWAL_DOKTER
                WHERE username='{request.session.get('username')}'
            """)
            
            var_dok = cursor.fetchall()
            for i in range(len(var_dok)):
                temp = list(var_dok[i])
                if temp[3] == 'pertama':
                    temp[3] = 'Shift 1'
                elif temp[3] == 'kedua':
                    temp[3] = 'Shift 2'
                elif temp[3] == 'ketiga':
                    temp[3] = 'Shift 3'
                elif temp[3] == 'keempat':
                    temp[3] = 'Shift 4'
                temp[4] = datetime.strftime(temp[4], '%d-%m-%Y')
                print(temp[4])
                var_dok[i] = temp
        except:
            traceback.print_exc()
    return render(request, "jadwal_dokter/index.html", {"var":var, "var_dok":var_dok})

def buat_jadwal(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM JADWAL EXCEPT (SELECT kode_faskes, shift, tanggal FROM JADWAL_DOKTER 
                WHERE username='{request.session.get("username")}')
            """)
            var = cursor.fetchall()
            for i in range(len(var)):
                temp = list(var[i])
                if temp[1] == 'pertama':
                    temp[1] = 'Shift 1'
                elif temp[1] == 'kedua':
                    temp[1] = 'Shift 2'
                elif temp[1] == 'ketiga':
                    temp[1] = 'Shift 3'
                elif temp[1] == 'keempat':
                    temp[1] = 'Shift 4'
                temp[2] = datetime.strftime(temp[2], '%d-%m-%Y')
                var[i] = temp
            return render(request, "jadwal_dokter/buat_jadwal.html", {"var":var})
        except:
            traceback.print_exc()
            return render(request, "jadwal_dokter/buat_jadwal.html")

def buat(request):
    with connection.cursor() as cursor:
        try:
            shift = ''
            if request.POST['shift'] == 'Shift 1':
                shift = 'pertama'
            elif request.POST['shift'] == 'Shift 2':
                shift = 'kedua'
            elif request.POST['shift'] == 'Shift 3':
                shift = 'ketiga'
            elif request.POST['shift'] == 'Shift 4':
                shift = 'keempat'
            
            cursor.execute(f"""
                SELECT username, nostr FROM DOKTER
                WHERE username='{request.session.get("username")}'
            """)

            dokter = cursor.fetchall()
            username = dokter[0][0]
            nostr = dokter[0][1]

            cursor.execute(f"""
                INSERT INTO JADWAL_DOKTER VALUES
                ('{nostr}', '{username}', '{request.POST['kode']}', '{shift}', '{request.POST['tanggal']}', 0)
            """)
            return redirect("jadwal_dokter:index")

        except:
            traceback.print_exc()
            print(shift)