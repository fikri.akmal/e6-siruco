from django.urls import path

from . import views

app_name = 'jadwal_dokter'

urlpatterns = [
    path('', views.index, name='index'),
    path('buat-jadwal/', views.buat_jadwal, name='buat_jadwal'),
    path('buat', views.buat, name='buat')
]
