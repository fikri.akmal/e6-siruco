from django.apps import AppConfig


class JadwalDokterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jadwal_dokter'
