from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from e6_siruco.auth_decorator import require_login


@require_login
def create_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        form = CreateFaskesForm()
        if request.method == "POST":
            kode = request.POST['kode']
            tipe = request.POST['tipe']
            nama = request.POST['nama']
            status = request.POST['status']
            jalan = request.POST['jalan']
            kelurahan = request.POST['kelurahan']
            kecamatan = request.POST['kecamatan']
            kabkot = request.POST['kabkot']
            provinsi = request.POST['provinsi']
            data_baru = [kode, tipe, nama, status, jalan,
                         kelurahan, kecamatan, kabkot, provinsi]
            with connection.cursor() as cursor:
                cursor.execute(
                    'INSERT INTO SIRUCO.FASKES VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)', data_baru)
            return redirect('faskes:read')
        return render(request, 'faskes/buat-faskes.html', {'form': form, 'peran': request.session['peran']})


@require_login
def update_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('kode') is not None:
                kode = request.GET.get('kode')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.FASKES where kode=%s LIMIT 1;', [kode])
                    data = dictfetchall(cursor)
                data_faskes = {}
                data_faskes['kode'] = data[0]['kode']
                data_faskes['tipe'] = data[0]['tipe']
                data_faskes['nama'] = data[0]['nama']
                data_faskes['status'] = data[0]['statusmilik']
                data_faskes['jalan'] = data[0]['jalan']
                data_faskes['kelurahan'] = data[0]['kelurahan']
                data_faskes['kecamatan'] = data[0]['kecamatan']
                data_faskes['kabkot'] = data[0]['kabkot']
                data_faskes['provinsi'] = data[0]['prov']
                form = UpdateFaskesForm(initial=data_faskes)
        else:
            kode = request.POST['kode']
            tipe = request.POST['tipe']
            nama = request.POST['nama']
            status = request.POST['status']
            jalan = request.POST['jalan']
            kelurahan = request.POST['kelurahan']
            kecamatan = request.POST['kecamatan']
            kabkot = request.POST['kabkot']
            provinsi = request.POST['provinsi']
            data_baru = [tipe, nama, status, jalan,
                         kelurahan, kecamatan, kabkot, provinsi, kode]
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE SIRUCO.FASKES SET tipe = %s, nama = %s,statusmilik = %s,jalan = %s,kelurahan = %s,kecamatan = %s,kabkot = %s,prov = %s WHERE KODE = %s", data_baru)
            return redirect('faskes:read')
        return render(request, 'faskes/update-faskes.html', {'form': form})


@require_login
def delete_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('kode') is not None:
                kode_faskes = request.GET.get('kode')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'DELETE FROM SIRUCO.FASKES WHERE kode = %s', [kode_faskes])
        return HttpResponseRedirect(reverse('faskes:read'))


@require_login
def read_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.FASKES")
            seluruh = dictfetchall(cursor)
        context = {'fsk': seluruh}
        return render(request, 'faskes/read-faskes.html', context)


@require_login
def detail_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        if request.method == 'GET':
            if request.GET.get('kode') is not None:
                kode_faskes = request.GET.get('kode')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'SELECT * FROM SIRUCO.FASKES where kode=%s LIMIT 1;', [kode_faskes])
                    data = dictfetchall(cursor)

        return render(request, 'faskes/detil-faskes.html', data[0])


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
