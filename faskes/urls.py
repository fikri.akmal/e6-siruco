from django.urls import path

from .views import delete_faskes, read_faskes, detail_faskes, update_faskes, create_faskes
app_name = 'faskes'

urlpatterns = [
    path('create/', create_faskes, name='create'),
    path('read/', read_faskes, name='read'),
    path('details/', detail_faskes, name='details'),
    path('update/', update_faskes, name='update'),
    path('delete/', delete_faskes, name='delete'),
]
