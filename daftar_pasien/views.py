from django.shortcuts import render, redirect
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from django.contrib import messages
import traceback

# Create your views here.
def index(request):
    return render(request, 'daftar_pasien/index.html')

def daftar(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM PASIEN
                WHERE nik = '{request.POST['nik']}'
            """)

            if (len(cursor.fetchall()) == 0):
                cursor.execute(f"""
                    INSERT INTO PASIEN VALUES
                    ('{request.POST['nik']}', '{request.session.get("username")}', '{request.POST['nama']}', 
                    '{request.POST['ktp_jalan']}', '{request.POST['ktp_kelurahan']}', '{request.POST['ktp_kecamatan']}', 
                    '{request.POST['ktp_kabkot']}', '{request.POST['ktp_prov']}', '{request.POST['dom_jalan']}', 
                    '{request.POST['dom_kelurahan']}', '{request.POST['dom_kecamatan']}', '{request.POST['dom_kabkot']}', 
                    '{request.POST['dom_prov']}', '{request.POST['notelp']}', '{request.POST['nohp']}')
                """)
            else:
                messages.add_message(request, messages.WARNING, f"Pasien dengan NIK {request.POST['nik']} sudah terdaftar di sistem!")
                return redirect("daftar_pasien:index")
        except:
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"Ada gangguan server internal, mohon coba lagi")
            return redirect("daftar_pasien:index")

    return redirect("daftar_pasien:list_pasien")

def list_pasien(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM PASIEN
                WHERE idpendaftar = '{request.session.get("username")}'
            """)

            pasien = cursor.fetchall()

            return render(request, 'daftar_pasien/list_pasien.html', {'pasien':pasien})
        except:
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"Ada gangguan server internal, mohon coba lagi")

def detail_pasien(request, nik):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM PASIEN
                WHERE idpendaftar='{request.session.get("username")}'
                AND nik='{nik}'
            """)

            pasien = cursor.fetchall()

            if len(pasien) == 0:
                messages.add_message(request, messages.WARNING, f"Pasien dengan NIK {nik} tidak ditermukan!")

            return render(request, 'daftar_pasien/detail_pasien.html', {'pasien':pasien})
        except:
            print("except")
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"Ada gangguan server internal, mohon coba lagi")
            return render(request, 'daftar_pasien/detail_pasien.html')

def delete_pasien(request, nik):
    if request.session.get('peran') != 'pengguna publik':
        return redirect('main:home')
    else:
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT idpendaftar FROM PASIEN
                    WHERE idpendaftar='{request.session.get("username")}'
                    AND nik='{nik}'
                """)
                pendaftar = cursor.fetchall()[0][0]
                cursor.execute(f"""
                    DELETE FROM PASIEN
                    WHERE idpendaftar='{request.session.get("username")}'
                    AND nik='{nik}'
                """)
                
                if len(pendaftar) != 0:
                    cursor.execute(f"""
                        DELETE FROM GEJALA_PASIEN
                        WHERE nik='{nik}'
                    """)
                    
                    cursor.execute(f"""
                        DELETE FROM MEMERIKSA
                        WHERE nik='{nik}'
                    """)

                return redirect("daftar_pasien:list_pasien")
            except:
                traceback.print_exc()
                return redirect("daftar_pasien:list_pasien")

def update_pasien(request, nik):
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM PASIEN
                    WHERE idpendaftar='{request.session.get("username")}'
                    AND nik='{nik}'
                """)

                if len(cursor.fetchall()) != 0:
                    cursor.execute(f"""
                        UPDATE PASIEN
                        SET ktp_jalan='{request.POST['ktp_jalan']}', ktp_kelurahan='{request.POST['ktp_kelurahan']}', 
                        ktp_kecamatan='{request.POST['ktp_kecamatan']}', ktp_kabkot='{request.POST['ktp_kabkot']}', 
                        ktp_prov='{request.POST['ktp_prov']}', dom_jalan='{request.POST['dom_jalan']}', 
                        dom_kelurahan='{request.POST['dom_kelurahan']}', dom_kecamatan='{request.POST['dom_kecamatan']}', 
                        dom_kabkot='{request.POST['dom_kabkot']}', dom_prov='{request.POST['dom_prov']}', 
                        notelp='{request.POST['notelp']}', nohp='{request.POST['nohp']}'
                        WHERE nik='{nik}' AND idpendaftar='{request.session.get("username")}'
                    """)
                    return redirect("daftar_pasien:list_pasien")
            
            except:
                traceback.print_exc()
                return redirect("daftar_pasien:list_pasien")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
                    SELECT * FROM PASIEN
                    WHERE idpendaftar='{request.session.get("username")}'
                    AND nik='{nik}'
                """)
        pasien = cursor.fetchall()

    return render(request, "daftar_pasien/update.html", {'pasien':pasien})
            