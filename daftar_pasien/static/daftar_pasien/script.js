function check_data() {
    var nik = document.getElementById('nikInput').value
    var nama = document.getElementById('namaInput').value
    var telp = document.getElementById('telpInput').value
    var nohp = document.getElementById('hpInput').value
    var jalanKtp = document.getElementById('jalanKtpInput').value
    var kelKtp = document.getElementById('kelKtpInput').value
    var kecKtp = document.getElementById('kecKtpInput').value
    var kabkotKtp = document.getElementById('kabkotKtpInput').value
    var provKtp = document.getElementById('provKtpInput').value
    var jalanDom = document.getElementById('jalanDomInput').value
    var kelDom = document.getElementById('kelDomInput').value
    var kecDom = document.getElementById('kecDomInput').value
    var kabkotDom = document.getElementById('kabkotDomInput').value
    var provDom = document.getElementById('provDomInput').value

    if (nik == "" || nama == "" || telp == "" || nohp == "" || 
        jalanKtp == "" || kelKtp == "" || kecKtp == "" || kabkotKtp == "" || provKtp == "" ||
        jalanDom == "" || kelDom == "" || kecDom == "" || kabkotDom == "" || provDom == "") {
            alert("Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu");
    }
}