from django.urls import path

from . import views

app_name = 'daftar_pasien'

urlpatterns = [
    path('', views.index, name='index'),
    path('daftar/', views.daftar, name='daftar'),
    path('list-pasien/', views.list_pasien, name='list_pasien'),
    path("detail-pasien/<int:nik>", views.detail_pasien, name='detail_pasien'),
    path("delete-pasien/<int:nik>", views.delete_pasien, name='delete_pasien'),
    path("update-pasien/<int:nik>", views.update_pasien, name='update_pasien')
]
