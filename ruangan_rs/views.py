from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.db import connection
from django.db.utils import IntegrityError, InternalError
from django.contrib import messages
from datetime import date
from datetime import datetime
import json
import traceback

# Create your views here.
def index(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT * FROM RUANGAN_RS
            """)
            ruangan_rs = cursor.fetchall()
        except:
            traceback.print_exc()
    return render(request, 'ruangan_rs/index.html', {'ruangan_rs':ruangan_rs})

def buat_ruangan(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kode_faskes FROM RUMAH_SAKIT
            """)
            kode_rs = cursor.fetchall()
        except:
            traceback.print_exc()
    return render(request, 'ruangan_rs/form_ruangan.html', {'kode_rs':kode_rs})

def kode_rs(request, kode):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT koderuangan FROM RUANGAN_RS
                WHERE koders='{kode}'
                ORDER BY koderuangan DESC
            """)
            kode_ruangan = cursor.fetchall()[0][0]
            kode_ruangan_one = kode_ruangan[0:2]
            kode_ruangan_two = int(kode_ruangan[2:])
            kode_ruangan_two += 1
            kode_ruangan_two = str(kode_ruangan_two).zfill(3)
            kode_ruangan = str(kode_ruangan_one) + str(kode_ruangan_two)
            return HttpResponse(json.dumps({'kode_ruangan':kode_ruangan}), content_type="application/json")
        except: 
            traceback.print_exc()

def buat(request):
    with connection.cursor() as cursor:
        try:
            if request.method == 'POST':
                cursor.execute(f"""
                    SELECT * FROM RUMAH_SAKIT
                    WHERE kode_faskes='{request.POST['kode-rs']}'
                """)
                if len(cursor.fetchall()) != 0:
                    cursor.execute(f"""
                        INSERT INTO RUANGAN_RS VALUES
                        ('{request.POST['kode-rs']}', '{request.POST['kode-ruangan']}', '{request.POST['tipe']}', 
                        0, '{request.POST['harga']}')
                    """)
                    return redirect('ruangan_rs:index')
                else:
                    messages.add_message(request, messages.WARNING, f"""Kode RS tidak valid""")
                    return redirect('ruangan_rs:buat-ruangan')
        except:
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"""Terdapat gangguan sistem""")
            return redirect('ruangan_rs:buat-ruangan')

def update_ruangan(request):
    try:
        if request.method == 'POST':
            kode_rs = request.POST['kode-rs']
            kode_ruangan = request.POST['kode-ruangan']
            return render(request, 'ruangan_rs/update_form.html', {'kode_rs':kode_rs, 'kode_ruangan':kode_ruangan})
    except:
        traceback.print_exc()
    return render(request, 'ruangan_rs/update_form.html', {'kode_rs':kode_rs, 'kode_ruangan':kode_ruangan})

def update(request):
    with connection.cursor() as cursor:
        try:
            if request.method == 'POST':
                cursor.execute(f"""
                    SELECT * FROM RUANGAN_RS
                    WHERE koderuangan='{request.POST['kode-ruangan']}' AND koders='{request.POST['kode-rs']}'
                """)
                ruangan = cursor.fetchall()
                if len(ruangan) != 0:
                    cursor.execute(f"""
                        UPDATE RUANGAN_RS
                        SET tipe='{request.POST['tipe']}', harga='{request.POST['harga']}'
                        WHERE koderuangan='{request.POST['kode-ruangan']}' AND koders='{request.POST['kode-rs']}'
                    """)
            return redirect('ruangan_rs:index')
        except:
            traceback.print_exc()

def bed_rs(request):
    with connection.cursor() as cursor:
        try:
            tanggal_kini = date.today()
            cursor.execute(f"""
                SELECT * FROM BED_RS
            """)
            bed_rs = cursor.fetchall()

            cursor.execute(f"""
                SELECT kodebed, tglkeluar FROM RESERVASI_RS
            """)
            bed_dipesan = []
            list_bed = cursor.fetchall()
            for bed in list_bed:
                print(type(bed[1]))
                if bed[1] > tanggal_kini:
                    bed_dipesan.append(bed[0])
        except:
            traceback.print_exc()
    return render(request, 'ruangan_rs/bed_rs.html', {'bed_rs':bed_rs, 'bed_dipesan':bed_dipesan})

def buat_bed(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kode_faskes FROM RUMAH_SAKIT
            """)
            kode_rs = cursor.fetchall()
        except:
            traceback.print_exc()
    return render(request, 'ruangan_rs/form_bed.html', {'kode_rs':kode_rs})

def kode_rs_bed(request, kode):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT koderuangan FROM RUANGAN_RS
                WHERE koders='{kode}'
                ORDER BY koderuangan ASC
            """)
            list_ruangan = cursor.fetchall()
            kode_ruangan = []
            for ruangan in list_ruangan:
                kode_ruangan.append(ruangan[0])

            return HttpResponse(json.dumps({'kode_ruangan':kode_ruangan}), content_type="application/json")
        except: 
            traceback.print_exc()

def kode_ruangan_bed(request, kode):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodebed FROM BED_RS
                WHERE koderuangan='{kode}'
                ORDER BY kodebed DESC
            """)
            try:
                kode_bed = cursor.fetchall()[0][0]
                if len(kode_bed) != 0:
                    kode_bed_one = kode_bed[0:2]
                    kode_bed_two = int(kode_bed[2:5]) + 1
                    kode_bed= kode_bed_one + str(kode_bed_two)
            except IndexError:
                no_ruangan = int(kode[2:])
                print(no_ruangan)
                print(no_ruangan >= 100)
                if no_ruangan >= 10 and no_ruangan < 100:  
                    kode_bed = 'A'
                    kode_bed += kode[1] + str(no_ruangan)
                    kode_bed += '1'
                elif no_ruangan >= 100:
                    kode_bed = ''
                    kode_bed = kode[1] + str(no_ruangan)
                    kode_bed += '1'
                else:
                    kode_bed = 'A'
                    kode_bed += kode[1] + str(no_ruangan)
                    kode_bed += '01'
            return HttpResponse(json.dumps({'kode_bed':kode_bed}), content_type="application/json")
        except:
            traceback.print_exc()

def tambah_bed(request):
    with connection.cursor() as cursor:
        try:
            if request.method == 'POST':
                cursor.execute(f"""
                    SELECT * FROM RUANGAN_RS
                    WHERE koderuangan='{request.POST['kode-ruangan-bed']}'
                """)
                if len(cursor.fetchall()) != 0:
                    cursor.execute(f"""
                        INSERT INTO BED_RS VALUES
                        ('{request.POST['kode-ruangan-bed']}', '{request.POST['kode-rs-bed']}', '{request.POST['kode-bed']}')
                    """)
                    return redirect('ruangan_rs:bed-rs')
                else:
                    messages.add_message(request, messages.WARNING, f"""Kode RS tidak valid""")
                    return redirect('ruangan_rs:tambah-bed')
        except ValueError:
            messages.add_message(request, messages.WARNING, f"""Kode RS dan Kode Ruangan tidak valid""")
            return redirect('ruangan_rs:tambah-bed')
        except:
            traceback.print_exc()
            messages.add_message(request, messages.WARNING, f"""Terdapat gangguan sistem""")
            return redirect('ruangan_rs:tambah-bed')

def delete_bed(request):
    if request.session.get('peran') != 'admin satgas':
        return redirect('main:home')
    else:
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT kodebed FROM RESERVASI_RS
                """)
                bed_dipesan = []
                list_bed = cursor.fetchall()
                for bed in list_bed:
                    bed_dipesan.append(bed[0])
                if request.POST['kode-bed'] not in bed_dipesan:
                    cursor.execute(f"""
                        DELETE FROM BED_RS
                        WHERE kodebed = '{request.POST['kode-bed']}' AND koderuangan='{request.POST['kode-ruangan']}'
                    """)
                    cursor.execute(f"""
                        UPDATE RUANGAN_RS
                        SET jmlbed = jmlbed - 1
                        WHERE koderuangan='{request.POST['kode-ruangan']}'
                    """)
                    return redirect('ruangan_rs:bed-rs')
                else:
                    messages.add_message(request, messages.WARNING, f"""Bed {request.POST['kode-bed']} sedang direservasi""")
                    return redirect('ruangan_rs:bed-rs')
            except:
                traceback.print_exc()

def kode_bed(request):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodebed FROM RESERVASI_RS
            """)
            bed_dipesan = []
            list_bed = cursor.fetchall()
            for bed in list_bed:
                bed_dipesan.append(bed[0])
            return HttpResponse(json.dumps({'bed_dipesan':bed_dipesan}), content_type="application/json")
        except:
            traceback.print_exc()