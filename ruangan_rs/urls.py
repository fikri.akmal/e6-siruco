from django.urls import path

from . import views

app_name = 'ruangan_rs'

urlpatterns = [
    path('', views.index, name='index'),
    path('buat-ruangan/', views.buat_ruangan, name='buat-ruangan'),
    path('kode-rs/<kode>', views.kode_rs, name='kode-rs'),
    path('buat/', views.buat, name='buat'),
    path('update-ruangan/', views.update_ruangan, name='update-ruangan'),
    path('update/', views.update, name='update'),
    path('bed-rs/', views.bed_rs, name='bed-rs'),
    path('buat-bed/', views.buat_bed, name='buat-bed'),
    path('bed-rs/kode-rs/<kode>', views.kode_rs_bed, name='kode-rs-bed'),
    path('bed-rs/kode-ruangan/<kode>', views.kode_ruangan_bed, name='kode-ruangan-bed'),
    path('tambah-bed/', views.tambah_bed, name='tambah-bed'),
    path('delete-bed/', views.delete_bed, name='delete-bed'),
    path('bed-rs/kode-bed/', views.kode_bed, name='kode-bed'),
]
