$(document).ready(function() {
    $('#kode-rs').change(function() {
        console.log($(this).val());
        var kode = $(this).val()
        $.ajax({
            url: '/ruangan-rs/kode-rs/' + kode,
            data: {
                'kode-rs': kode
            },
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                document.getElementById('kode-ruangan').value = data.kode_ruangan;
                console.log(data.kode_ruangan)
            }
        })
    });

    $('#kode-rs-bed').change(function() {
        $("#kode-ruangan-bed option").remove();
        $("#kode-ruangan-bed").append('<option value="">Kode Ruangan</option>');
        console.log($(this).val());
        var kode = $(this).val();
        $.ajax({
            url: '/ruangan-rs/bed-rs/kode-rs/' + kode,
            data: {
                'kode-rs': kode
            },
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                for (let i = 0; i < data.kode_ruangan.length; i++) {
                    var html = '<option value="'
                    html += data.kode_ruangan[i]
                    html += '">'
                    html += data.kode_ruangan[i]
                    html += '</option>'
                    $('#kode-ruangan-bed').append(html);
                    console.log(data.kode_ruangan[i]);
                }
            }
        })
    });

    $('#kode-ruangan-bed').change(function() {
        var kode = $(this).val();
        $.ajax({
            url: '/ruangan-rs/bed-rs/kode-ruangan/' + kode,
            data: {
                'kode-ruangan': kode
            },
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#kode-bed').val(data.kode_bed);
                console.log(data.kode_bed);
            }
        });
    });
});