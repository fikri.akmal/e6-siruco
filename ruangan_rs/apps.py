from django.apps import AppConfig


class RuanganRsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ruangan_rs'
