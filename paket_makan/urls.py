from os import name
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('buat-paket-makan/', create_paket_makan, name='buat-paket-makan'),
    path('list-paket-makan/', list_paket_makan, name='list-paket-makan'),
    path(
        'ubah-paket-makan/<str:kodehotel>/<str:kodepaket>/',
        ubah_paket_makan,
        name='ubah-paket-makan'
    ),
    path('hapus-paket-makan/', hapus_paket_makan, name='hapus-paket-makan')
]
