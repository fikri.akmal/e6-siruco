from django.http.response import HttpResponse, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from e6_siruco.db_conn_util import open_connection
from e6_siruco.auth_decorator import require_login
from django.shortcuts import redirect, render


# Create your views here.


# -------------- CREATE View
@csrf_exempt
@require_login
def create_paket_makan(request):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute("SET search_path to siruco;")

    if request.method == 'POST':
        kodehotel = request.POST.get('kodehotel')
        kodepaket = request.POST.get('kodepaket')
        namapaket = request.POST.get('namapaket')
        harga = int(request.POST.get('harga'))

        # Ngecek ada paket makan yang kodepaketnya sama apa engga
        cursor.execute(
            f"SELECT * FROM paket_makan where kodehotel = '{kodehotel}' and kodepaket = '{kodepaket}';"
        )

        isExist = True if len(cursor.fetchall()) > 0 else False

        if isExist:
            conn.close()
            return HttpResponseNotAllowed()

        # INSERT paket makan baru
        cursor.execute(
            f"""
            INSERT INTO paket_makan VALUES (
            '{kodehotel}', '{kodepaket}', '{namapaket}', '{harga}');
            """
        )
        conn.commit()

        conn.close()
        
        return HttpResponse("Succeed.")
        
    
    if request.session.get('peran') == 'admin sistem':
        cursor.execute('SELECT kode FROM hotel;')
        list_kode = cursor.fetchall()

        context = {
            'list_kode': list_kode
        }

        conn.close()

        return render(request, 'buat-paket-makan.html', context)
    
    elif request.session.get('peran').lower() == 'pengguna publik' or\
         request.session.get('peran').lower() == 'admin satgas':

         conn.close()
         return redirect("main:home")


# -------------- READ View

@require_login
def list_paket_makan(request):
    conn = open_connection()
    cursor = conn.cursor()
    cursor.execute('SET search_path to siruco;')
    context = {}

    cursor.execute('SELECT * FROM paket_makan')
    list_paket = cursor.fetchall()

    context['list_paket'] = list_paket

    if request.session.get('peran').lower() == 'admin sistem':
        context['isAdminSistem'] = True
    else:
        context['isAdminSistem'] = False

    conn.close()

    return render(request, 'list-paket-makan.html', context)


# --------------- UPDATE View
@csrf_exempt
@require_login
def ubah_paket_makan(request, kodehotel, kodepaket):
    if request.session.get('peran', False) == 'admin sistem':
        if request.method == 'POST':

            namapaket = request.POST.get('namapaket')
            kode_hotel = request.POST.get('kodehotel')
            harga = request.POST.get('harga')
            kode_paket = request.POST.get('kodepaket')

            conn = open_connection()
            cursor = conn.cursor()
            cursor.execute('SET search_path to siruco;')

            # Query update paket makan
            cursor.execute(
                f"""
                UPDATE paket_makan SET 
                nama = '{namapaket}', harga = {harga}
                WHERE kodehotel = '{kode_hotel}' AND
                kodepaket = '{kode_paket}';
                """
            )

            conn.commit()
            conn.close()
            return redirect('list-paket-makan')

        context = {
            'kodehotel': kodehotel,
            'kodepaket': kodepaket
        }

        return render(request, 'ubah-paket-makan.html', context)

    return redirect('main:home')



# --------------- DELETE View
@csrf_exempt
@require_login
def hapus_paket_makan(request):
    if request.method == "POST":
        if request.session.get('peran', False) == 'admin sistem':
            kodehotel = request.POST.get('kodehotel')
            kodepaket = request.POST.get('kodepaket')

            conn = open_connection()
            cursor = conn.cursor()
            cursor.execute('SET search_path to siruco;')

            # Ngecek udah pernah diorder apa belom
            cursor.execute(
                f"""
                SELECT * FROM daftar_pesan
                WHERE kodehotel = '{kodehotel}' AND
                kodepaket = '{kodepaket}';
                """
            )

            res = cursor.fetchall()
            isOrdered = True if len(res) > 0 else False

            if (isOrdered):
                return HttpResponseNotAllowed()

            # query delete
            cursor.execute(
                f"""
                DELETE from paket_makan
                WHERE kodehotel = '{kodehotel}'
                AND kodepaket = '{kodepaket}';
                """
            )

            conn.commit()
            conn.close()

            return HttpResponse("Succeed.")

    return HttpResponseNotAllowed()


