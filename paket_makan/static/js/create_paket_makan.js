$('#form').submit(function(event) {
    event.preventDefault();
    let kodehotel = $('#kodehotel').val();
    let kodepaket = $('#kodepaket').val();
    let namapaket = $('#namapaket').val();
    let harga = $('#harga').val();

    $.ajax({
        url:'/paket-makan/buat-paket-makan/',
        method:'POST',
        data: {
            'kodehotel': kodehotel,
            'kodepaket': kodepaket,
            'namapaket': namapaket,
            'harga': harga                                                                                 
        }, 
        success: (e) => {
            alert('Paket berhasil dibuat!');
            window.location.pathname = '/paket-makan/list-paket-makan';
        },
        error: (e) => {
            alert('Kode paket sudah ada!');
        }
    })
})