// Update Paket
$('.btn-update').on('click', function() {
    let list = $(this).val().split("-");
    window.location.pathname = `/paket-makan/ubah-paket-makan/${list[0]}/${list[1]}/`;
});

// Hapus Paket
$('.btn-delete').click(function() {
    let action = confirm("Apakah kamu yakin akan mendelete paket ini?");
    if (action) {
        let kodehotel = $(this).val().split("-")[0];
        let kodepaket = $(this).val().split("-")[1];
        $.ajax({
            method: "POST",
            url:'/paket-makan/hapus-paket-makan/',
            data: {
                'kodehotel': kodehotel,
                'kodepaket': kodepaket
            }, 
            success: (e) => {
                alert('Paket berhasil dihapus');
                location.reload()
            }, error: (e) => {
                alert('Paket sudah ada yang membeli!');
            }
        });
    };
});