from django.urls import path

from .views import create_jadwal_faskes, read_jadwal_faskes
app_name = 'jadwal_faskes'

urlpatterns = [
    path('create/', create_jadwal_faskes, name='create'),
    path('read/', read_jadwal_faskes, name='read'),
]
