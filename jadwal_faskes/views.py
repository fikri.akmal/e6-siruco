
from django.shortcuts import render

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from django.db import connection
from e6_siruco.auth_decorator import require_login


@require_login
def create_jadwal_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        form = CreateJadwalFaskesForm()
        if request.method == "POST":
            kode_faskes = request.POST['faskes']
            shift = request.POST['shift']
            tanggal = request.POST['tanggal']
            data_baru = [kode_faskes, shift, tanggal]
            with connection.cursor() as cursor:
                cursor.execute(
                    'INSERT INTO SIRUCO.JADWAL VALUES (%s, %s, %s)', data_baru)
            return redirect('jadwal_faskes:read')
        return render(request, 'jadwal_faskes/buat-jadwal-faskes.html', {'form': form, 'peran': request.session['peran']})


@require_login
def read_jadwal_faskes(request):
    if request.session.get('peran', False) == 'admin satgas':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM SIRUCO.JADWAL")
            seluruh = dictfetchall(cursor)
        context = {'fsk': seluruh}
        return render(request, 'jadwal_faskes/read-jadwal-faskes.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
